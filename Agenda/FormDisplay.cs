﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Agenda
{
    class FormDisplay
    {
        public FormDisplay(DataGridView dataGrid)
        {
            dataGrid.Rows.Clear();
            this.dataGrid = dataGrid;
        }

        private DataGridView dataGrid;
        
        public void AddItem(Person person)
        {            
            dataGrid.Rows.Add(person.user, person.phone, person.mail);
        }

        public Person DeleteItem()
        {
            Person person = new Person();
            foreach (DataGridViewCell oneCell in dataGrid.SelectedCells)
            {
                if (oneCell.Selected)
                {
                    person.user = oneCell.Value.ToString();
                    dataGrid.Rows.RemoveAt(oneCell.RowIndex);
                }
            }
            return person;
        }

        public void UpdateItem(Person person)
        {
            foreach (DataGridViewCell oneCell in dataGrid.SelectedCells)
            {
                if (oneCell.Selected)
                {
                    int row = dataGrid.SelectedCells[0].RowIndex;
                    dataGrid.Rows[row].Cells[0].Value = person.user;
                    dataGrid.Rows[row].Cells[1].Value = person.phone;
                    dataGrid.Rows[row].Cells[2].Value = person.mail;
                }
            }
        }

        public void Filter(int indexRow, int indexColumn)
        {
            dataGrid.ClearSelection();
            dataGrid.Rows[indexRow].Cells[indexColumn].Selected = true;
        }

        public void DisplayData(List<Person> person)
        {
            foreach (var item in person)
            {
                dataGrid.Rows.Add(item.user, item.phone, item.mail);
            }             
        }
    }
}
