﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Agenda
{
    public class Controller
    {
        //List <Person> listPerson = new List<Person>();
        Database db = new Database();
        FormDisplay fd;
        AgendaForm form;

        public Controller(AgendaForm view)
        {
            fd = new FormDisplay(view.dataGrid);
            form = view;
        }

        public void AddItem(Person person)
        {

            fd.AddItem(person);

            db.AddItem(person);
        }

        public void Filter(string user)
        {
            int indexRow = 0;
            foreach (Person p in db.GetItems())
            {
                if(p.user == user)
                {
                    fd.Filter(indexRow, 0);

                    form.SelectedTextBox(p);
                }
                indexRow++;
            }
        }

        public void DisplayData()
        {            
            fd.DisplayData(db.GetItems()); 
        }

        public void DeleteItem()
        {
            db.DeleteItem(fd.DeleteItem());
        }

        public void UpdateItem()
        {
            fd.UpdateItem(form.GetPerson());
            db.UpdateItem(form.GetPerson());
        }

        public bool CheckName(string name)
        {
            foreach (var item in db.GetItems())
            {
                if(name == item.user)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
