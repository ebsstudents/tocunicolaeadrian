﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Agenda
{
    public partial class AgendaForm : Form
    {
        Controller ctrl;        

        public AgendaForm()
        {
            Person p = new Person();
            InitializeComponent();
            //CheckForIllegalCrossThreadCalls = false; //criminala
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ctrl.DisplayData();
        }

        public void  SetController(Controller controller)
        {
            ctrl = controller;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Person p = new Person();

            p.user   = nameTextBox.Text;
            p.phone  = phoneTextBox.Text;
            p.mail   = mailTextBox.Text;

            ctrl.AddItem(p);
            Success(successfullyLabel); //DELETE THIS ONE            
        }

        public void SelectedTextBox(Person person)
        {
            nameTextBox.Text  = person.user;
            phoneTextBox.Text = person.phone;
            mailTextBox.Text  = person.mail;
        }

        public Person GetPerson()
        {
            Person person = new Person();

            person.user  = nameTextBox.Text;
            person.phone = phoneTextBox.Text;
            person.mail  = mailTextBox.Text;

            return person;
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            ctrl.Filter(filterTextBox.Text);
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            ctrl.DeleteItem();
            Success(successfullyLabel); //DELETE THIS ONE
        }

        private void dataGrid_SelectionChanged(object sender, EventArgs e)
        {
            //delete this one later
        }

        private void dataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                nameTextBox.Text = dataGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                phoneTextBox.Text = dataGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                mailTextBox.Text = dataGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            ctrl.UpdateItem();
            Success(successfullyLabel); //DELETE THIS ONE
        }

        //Visual settings ! ! ! having fun ^^

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            NameValidation(nameTextBox.Text);
            UnlockTheButtons();
        }

        private void nameTextBox_VisibleChanged(object sender, EventArgs e)
        {
            NameValidation(nameTextBox.Text);
            UnlockTheButtons();
        }


        private void phoneTextBox_TextChanged(object sender, EventArgs e)
        {
            PhoneValidation(phoneTextBox.Text);
            UnlockTheButtons();
        }

        private void phoneTextBox_VisibleChanged(object sender, EventArgs e)
        {
            PhoneValidation(phoneTextBox.Text);
            UnlockTheButtons();
        }


        private void mailTextBox_TextChanged(object sender, EventArgs e)
        {
            MailValidation(mailTextBox.Text);
            UnlockTheButtons();
        }

        private void mailTextBox_VisibleChanged(object sender, EventArgs e)
        {
            MailValidation(mailTextBox.Text);
            UnlockTheButtons();
        }  


        //Validations

        private void MailValidation(string mail)
        {
            RegexUtilities util = new RegexUtilities();
            if (util.IsValidEmail(mail))
            {
                mailLabel.ForeColor = Color.Green;
            }
            else
            {
                mailLabel.ForeColor = Color.Red;
            }
        }

        private void PhoneValidation(string phone)
        {
            RegexUtilities util = new RegexUtilities();
            if(util.IsPhoneNumber(phone))
            {
                phoneLabel.ForeColor = Color.Green;
            }
            else
            {
                phoneLabel.ForeColor = Color.Red;
            }
        }

        private void NameValidation(string name)
        {
            if(ctrl.CheckName(name))
            {
                nameLabel.ForeColor = Color.Green;
            }
            else
            {
                nameLabel.ForeColor = Color.Red;
            }
        }

        private void UnlockTheButtons()
        {
            if(nameLabel.ForeColor == Color.Green && phoneLabel.ForeColor == Color.Green && mailLabel.ForeColor == Color.Green)
            {
                addButton.Enabled = true;
            }
            else
            {
                addButton.Enabled = false;
            }
        }

        //NOT SAFE FOR WORK 
        
        private void ShowLabel(Object label)
        {
            //var lab = label as Label;
            var lab = (Label)label;
            Thread.Sleep(1000);
            this.Invoke(new Action(() => {
                lab.Visible = false;
            }));
        }

        private void Success(Label label)
        {
            label.Visible = true;
            label.ForeColor = Color.Green;
            Thread changeUntil = new Thread(new ParameterizedThreadStart(ShowLabel));
            changeUntil.Start(label);            
        }
   
         
    }
}
