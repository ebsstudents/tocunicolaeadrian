﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Agenda
{
    class Database
    {
        string path = @"C:\Users\Adrian\Documents\Visual Studio 2013\Projects\Agenda\database.txt";

        public void AddItem(Person person)
        {
            using (StreamWriter sw = new StreamWriter(path, true))
            {
                sw.WriteLine(person.user + "|" + person.phone + "|" + person.mail);
                sw.Flush();
            }
        }
        
        public void UpdateItem(Person person)
        {
            DeleteItem(person);
            AddItem(person);
        }

        public void DeleteItem(Person person)
        {
            string deleteLine = person.user;
            File.Copy(path, @"C:\Users\Adrian\Documents\Visual Studio 2013\Projects\Agenda\tempFile.txt");
            string tempPath = @"C:\Users\Adrian\Documents\Visual Studio 2013\Projects\Agenda\tempFile.txt";

            using (StreamReader reader = new StreamReader(tempPath))
            {   
                using (StreamWriter writer = new StreamWriter(path))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var lineWords = line.Split('|');

                        if (String.Compare(lineWords[0], deleteLine) == 0)
                            continue;

                        writer.WriteLine(line);
                    }
                }
            }
            if (System.IO.File.Exists(tempPath))
            {
                try
                {
                    System.IO.File.Delete(tempPath);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
            }
        }

      /*  public bool CheckName(string name)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                while(!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var lineWords = line.Split('|');

                    if(String.Compare(lineWords[0], name) == 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
       */

        public List<Person> GetItems()
        {
            var personList = new List<Person>();
            var lines = System.IO.File.ReadAllLines(path);

            foreach (var item in lines)
            {
                var person = new Person();
				var details = item.Split('|');

                person.user  = details[0];
                person.phone = details[1];
                person.mail  = details[2];

                personList.Add(person);
            }			
			return personList;        
        }
    }
}
