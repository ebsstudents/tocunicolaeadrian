﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AgendaForm appView = new AgendaForm();
            Controller ctrl = new Controller(appView);
            appView.SetController(ctrl);
            Application.Run(appView);
        }
    }
}
